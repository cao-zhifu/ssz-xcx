/**
 * 封装网络请求
 */

import indexConfig from '@/config/index.config'
import state from '@/store/index.js'
module.exports = (params) => {
	let url = '';
	url = indexConfig.baseUrl + params.url; // 线上环境中开启
	// let url = "/dpc/" + params.url;									// 本地访问时开启
	let method = params.method;
	let header = params.header || {};
	let data = params.data || {};

	// 请求方式 get post
	if (method) {
		method = method.toUpperCase(); // 小写转大写
		if (method == "POST" || "GET" || "PUT") {
			header = {
				'Content-Type': 'application/json',
				'x-token':state.getters.getToken,
				
				
			};
		}
	}
	header={
		'Content-Type': 'application/json',
		'x-token':state.getters.getToken,
	}
	//	发起请求 加载动画
	if (!params.hideLoading) {
		uni.showNavigationBarLoading()
	}

	//	发起网络请求
	uni.request({
		url: url,
		data: data,
		dataType: 'json',	 // 'json' : 'sting'
		sslVerify: false,
		method: method || "GET",
		header: header,
		success: res => {
			if (res.code && res.code != 20000) {
				//	api错误
				uni.showModal({
					// content: res.msg
					content: '网络错误, 请稍后再试~'
				})
				return;
			}
			typeof params.success == "function" && params.success(res.data);
		},
		fail: err => {
			uni.showModal({
				content: '加载失败, 稍后再试~'
			})
			typeof params.fail == "function" && params.fail(err.data);
		},
		complete: (e) => {
			// console.log("请求完成", e);
			uni.hideNavigationBarLoading()
			typeof params.complete == "function" && params.complete(e.data);
			return;
		}
	})
}
