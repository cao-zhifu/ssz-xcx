const CONFIG = {
	// 开发环境配置
	development: {
		assetsPath: '/static', // 静态资源路径
		baseUrl: 'http://localhost:8001/user',
		 // 后台接口请求地址
		// baseUrl: '2408:841c:6530:dd17:c5d7:752d:d251:a6b9:8001/user',
		 // 后台接口请求地址
		
		hostUrl: '', // H5地址(前端运行地址)
		websocketUrl: '', // websocket服务端地址
		weixinAppId: '' // 微信公众号appid
	},
	// 生产环境配置
	production: {
		assetsPath: '/static', // 静态资源路径
		baseUrl: 'http://localhost:8001/user', // 后台接口请求地址
		hostUrl: '', // H5地址(前端运行地址)
		websocketUrl: '', // websocket服务端地址
		weixinAppId: '' // 微信公众号appid
	}

};
export default CONFIG[process.env.NODE_ENV];